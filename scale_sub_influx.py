#!/usr/bin/env python

import signal
import json
import time
import paho.mqtt.client as mqtt
import influxdb

BROKER_URL = "iqueue.ics.uci.edu"
BROKER_PORT = 1883
SUB_TOPIC = "iot-1/d/b827eb54155c/evt/+/json"

DB_HOST = "localhost"
DB_PORT = 8086
DB_NAME = "scale_cali"

influx = influxdb.InfluxDBClient(DB_HOST, DB_PORT)
influx.create_database(DB_NAME)
influx.switch_database(DB_NAME)

def on_connect(client, userdata, flags, rc):
    print("Connected: rc=%d" % rc)
    client.subscribe(SUB_TOPIC)

def on_message(client, userdata, msg):
    try:
        json_msg = json.loads(str(msg.payload))
    except ValueError:
        print(msg.topic + ": " + "Unable to decode message as JSON object")
        return

    if not "d" in json_msg:
        print(msg.topic + ": " + "Unable to decode message as SCALE event")
        return
    msg_d = json_msg["d"]
    
    if         not "timestamp" in msg_d \
            or not "value" in msg_d \
            or not "event" in msg_d:
        print(msg.topic + ": " + "Cannot find one or more field(s)")
        return
    evt_ts = int(msg_d["timestamp"] * 1000000000)
    evt_measurement = msg_d["event"]
    value_type = type(msg_d["value"]).__name__

    if          not type(msg_d["value"]) == type("") \
            and not type(msg_d["value"]) == type(u"") \
            and not type(msg_d["value"]) == type(True) \
            and not type(msg_d["value"]) == type(4) \
            and not type(msg_d["value"]) == type(4L) \
            and not type(msg_d["value"]) == type(4.):
        print(msg.topic + ": " + value_type + " is not supported")
        return

    evt_tags = {"device": msg.topic.split("/")[2], "value_type": value_type}
    evt_fields = {value_type + "_value": msg_d["value"]}

    if "prio_value" in msg_d:
        evt_fields["prio_value"] = msg_d["prio_value"]
    
    data_point = {"measurement": evt_measurement, "time": evt_ts, "tags": evt_tags, "fields": evt_fields}
    data_body = [data_point]
    try:
        influx.write_points(data_body)
        print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(msg_d["timestamp"])) + ": " + msg.topic)
    except influxdb.exceptions.InfluxDBServerError as err:
        print(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(msg_d["timestamp"])) + ": " + msg.topic + ", failed writing to DB")

def on_disconnect(client, userdata, rc):
    print("Disconnected: rc=%d" % rc)

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.on_disconnect = on_disconnect

print("Connecting to broker: %s, port=%d" % (BROKER_URL, BROKER_PORT))
client.connect(BROKER_URL, BROKER_PORT, 60)

signal.signal(signal.SIGINT, signal.default_int_handler)

try:
    client.loop_forever()
except KeyboardInterrupt:
    print("")
print("Terminated")


#!/usr/bin/env python

import signal
import paho.mqtt.client as mqtt

BROKER_URL = "iqueue.ics.uci.edu"
BROKER_PORT = 1883
SUB_TOPIC = "iot-1/d/b827eb54155c/evt/+/json"

def on_connect(client, userdata, flags, rc):
    print("Connected: rc=%d" % rc)
    client.subscribe(SUB_TOPIC)

def on_message(client, userdata, msg):
    print(msg.topic + ", " + str(msg.payload))

def on_disconnect(client, userdata, rc):
    print("Disconnected: rc=%d" % rc)

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.on_disconnect = on_disconnect

print("Connecting to broker: %s, port=%d" % (BROKER_URL, BROKER_PORT))
client.connect(BROKER_URL, BROKER_PORT, 60)

signal.signal(signal.SIGINT, signal.default_int_handler)

try:
    client.loop_forever()
except KeyboardInterrupt:
    print("")
print("Terminated")

